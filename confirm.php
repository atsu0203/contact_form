<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>
  <?php
  require "function.php";
  session_start();
  validation($_POST);
  $_SESSION = $_POST;
  //データベースに接続
  ?>
  <body>
    <h1>入力内容を確認してください</h1>
    <form action="complete.php" method="post">
      <label for="">名前:</label>
      <?php echo htmlspecialchars($_SESSION["name"], ENT_QUOTES, "UTF-8"); ?><br>
      <label for="">性別:</label>
      <?php echo htmlspecialchars($_SESSION["gender"], ENT_QUOTES, "UTF-8"); ?><br>
      <label for="">住所:</label>
      <?php echo htmlspecialchars($_SESSION["email"], ENT_QUOTES, "UTF-8"); ?><br>
      <label for="">内容:</label>
      <?php echo htmlspecialchars($_SESSION["content"], ENT_QUOTES, "UTF-8"); ?><br>
      <br>

      <input type="button" onclick="history.back();" value="戻る">
      <input type="submit" value="送信する">
    </form>
  </body>
</html>

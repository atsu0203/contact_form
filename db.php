<?php
//管理者登録
function admindb(){
$dsn = "mysql:dbname=test;host=localhost;charset=utf8";
$user = "root";
$password = "";
//接続チェック
try{
  $db = new PDO($dsn, $user, $password);
//echo "接続成功";
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
//存在しないテーブル名やカラム名をSQL文にもつプリペアードステートメントを発行したとき、エミュレーションお風っであればエラー
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが怒った際　例外をスローに
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//固定値で配置した値の部分をプレイスホルダーにする
$sql = "INSERT INTO admin(id, name, password, email) VALUES(NULL,:name, :pass, :email)";
$stmt = $db->prepare($sql);
//bind機構を使用して:userName 部分に変数$userNameに代入されている値を当てはめる
$name = $_SESSION["adminName"];
$pass = password_hash($_SESSION["adminPass"],PASSWORD_BCRYPT);
$email = $_SESSION["adminEmail"];
$stmt->bindParam(':name', $name, PDO::PARAM_STR);
$stmt->bindParam(':pass', $pass, PDO::PARAM_STR);
$stmt->bindParam(':email', $email, PDO::PARAM_STR);
$stmt-> execute();
}

//管理者ログイン
function logindb(){
$dsn = "mysql:dbname=test;host=localhost;charset=utf8";
$user = "root";
$password = "";
try{
  $db = new PDO($dsn, $user, $password);
  // echo "接続成功";
  }
  catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
  }
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが怒った際　例外をスローに
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = "SELECT * FROM admin WHERE email=:email";
$stmt = $db->prepare($sql);
$email = $_SESSION["adminEmail"];
$stmt->bindParam(':email', $email);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
  if(password_verify($_SESSION['adminPass'], $result['Password'])){
    // echo "ログイン認証に成功しました";
  }else{
    header('location: login.php');
  }
}




//db問い合わせ登録
function db(){
$dsn = "mysql:dbname=test;host=localhost;charset=utf8";
$user = "root";
$password = "";
//接続チェック
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
//存在しないテーブル名やカラム名をSQL文にもつプリペアードステートメントを発行したとき、エミュレーションお風っであればエラー
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが怒った際　例外をスローに
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//固定値で配置した値の部分をプレイスホルダーにする
$sql = "INSERT INTO contact_form(id, name, gender, email, content)VALUES(NULL,:name, :gender, :email, :content)";
$stmt = $db->prepare($sql);
//bind機構を使用して:userName 部分に変数$userNameに代入されている値を当てはめる
$name = $_SESSION["name"];
$email = $_SESSION["email"];
$gender = $_SESSION["gender"];
$content = $_SESSION["content"];
$stmt->bindParam(':name', $name, PDO::PARAM_STR);
$stmt->bindParam(':gender', $gender, PDO::PARAM_STR);
$stmt->bindParam(':email', $email, PDO::PARAM_STR);
$stmt->bindParam(':content', $content, PDO::PARAM_STR);
$stmt-> execute();
}



//db問い合わせ一覧受信
function listdb(){
$dsn = "mysql:dbname=test;host=localhost;charset=utf8";
$user = "root";
$password = "";
//接続チェック
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
//存在しないテーブル名やカラム名をSQL文にもつプリペアードステートメントを発行したとき、エミュレーションお風っであればエラー
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが怒った際　例外をスローに
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//固定値で配置した値の部分をプレイスホルダーにする
$sql = "SELECT * FROM contact_form";
$stmt = $db->prepare($sql);
$stmt-> execute();
global $result;
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
}


//問い合わせ削除
function deletedb(){
$dsn = "mysql:dbname=test;host=localhost;charset=utf8";
$user = "root";
$password = "";
//接続チェック
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
//存在しないテーブル名やカラム名をSQL文にもつプリペアードステートメントを発行したとき、エミュレーションお風っであればエラー
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが怒った際　例外をスローに
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// DELETE文を変数に格納
$sql = "DELETE FROM contact_form WHERE id = :id";
// 削除するレコードのIDは空のまま、SQL実行の準備をする
$stmt = $db->prepare($sql);
// 削除するレコードのIDを配列に格納する
$id = $_SESSION["id"];
$stmt->bindParam(':id', $id, PDO::PARAM_STR);
// 削除するレコードのIDが入った変数をexecuteにセットしてSQLを実行
$stmt->execute();
}



//問い合わせ修正
function editdb(){
$dsn = "mysql:dbname=test;host=localhost;charset=utf8";
$user = "root";
$password = "";
//接続チェック
try{
  $db = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
  echo "接続失敗" .$e->getMessage(). "\n";
}
//存在しないテーブル名やカラム名をSQL文にもつプリペアードステートメントを発行したとき、エミュレーションお風っであればエラー
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
//SQLでエラーが怒った際　例外をスローに
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// UPDATE文を変数に格納
$sql = "UPDATE contact_form SET name = :name, email = :email WHERE id = :id";
// 修正するレコードのIDは空のまま、SQL実行の準備をする
$stmt = $db->prepare($sql);
// 修正するレコードのIDを配列に格納する
$name = $_SESSION["name"];
$email = $_SESSION["email"];
$id = $_SESSION["id"];
$stmt->bindParam(':name', $name, PDO::PARAM_STR);
$stmt->bindParam(':email', $email, PDO::PARAM_STR);
$stmt->bindParam(':id', $id, PDO::PARAM_STR);
// 修正するレコードのIDが入った変数をexecuteにセットしてSQLを実行
$stmt->execute();
}
?>

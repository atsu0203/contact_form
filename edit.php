<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>修正画面</title>
  </head>

  <?php
  session_start();
  if(!empty($_SESSION["edit"])){
  }else{
    $_SESSION =$_POST;
  }
  if (empty($_SESSION["id"])){
    header('location: list.php');
  }
  ?>


  <body>
    <h1>修正画面</h1>
      <form action='edit_confirm.php' method='POST'>
        <input type="hidden" name="id" value="<?php echo $_SESSION['id'];?>">
        <p><span class="error"><?php if(isset($_SESSION["nameErr"])){echo $_SESSION["nameErr"];}?></span></p>
        <p>Name<input type="text" name="name"value="<?php if(!empty($_SESSION['name'])){ echo $_SESSION['name']; } ?>"></p>
        <p>Email<span class="error"><?php if(isset($_SESSION["emailErr"])){echo $_SESSION["emailErr"];}?></span></p>
        <p><input type="text" name="email" value="<?php if(!empty($_SESSION['email'])){ echo $_SESSION['email'];}?>"></p>
      <a href="list.php"><button type="button">もどる</button></a>　
      <input type="submit"  name="edit" value="修正する">
    </form>
  </body>
</html>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>管理者登録</title>
  </head>

  <?php
  session_start();
  $_SESSION = $_POST;
  //var_dump($_SESSION);
  if (!empty($_SESSION["adminEmail"])) {
    echo 'ログアウトしました';
  }
  //セッション変数のクリア
  $_SESSION = array();
  //セッションクッキーも削除
  if (ini_get("session.use_cookies")) {
      $params = session_get_cookie_params();
      setcookie(session_name(), '', time() - 42000,
          $params["path"], $params["domain"],
          $params["secure"], $params["httponly"]
      );
  }
  //セッションクリア
  @session_destroy();
  ?>


  <body>
    <h1> 管理者登録</h1>
    <form action='list.php' method='POST'>

      <span class="error"><?php if(isset($_SESSION["nameErr"])){echo $_SESSION["nameErr"];}?></span>
      <p>name:<input type="text" name="adminName"value="<?php if(!empty($_SESSION['adminName'])){ echo $_SESSION['adminName']; } ?>"><br></p>

      <span class="error"><?php if(isset($_SESSION["emailErr"])){echo $_SESSION["emailErr"];}?></span>
      <p>Email:<input type="text" name="adminEmail" value="<?php if(!empty($_SESSION['adminEmail'])){ echo $_SESSION['adminEmail'];}?>"><br></p>

      <span class="error"><?php if(isset($_SESSION["passErr"])){echo $_SESSION["passErr"];}?></span>
      <p>Password:<span class="error"><input type="text" name="adminPass" value="<?php if(!empty($_SESSION['adminPass'])){ echo $_SESSION['adminPass']; } ?>"><br></p>

      <input type="submit"  name="register" value="登録する">
      <a href="login.php"><button type="button">ログインページへ</button></a>
    </form>
  </body>
</html>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>削除確認</title>
  </head>

  <?php
  session_start();
  $_SESSION = $_POST;

  if (empty($_SESSION["id"])||empty($_SESSION["name"])||empty($_SESSION["email"])){
    header('location: list.php');
  }
  ?>
  <body>
    <h1> 本当に削除しますか</h1>
    <a href="list.php"><button type="button">もどる</button></a>　
    <form action='delete_complete.php' method='POST'>
    <input type="submit"  name="delete" value="削除する">
    </form>
  </body>
</html>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>修正の確認</title>
  </head>

  <?php
  require "function.php";
  require "db.php";
  session_start();
  editvalidation($_POST);
  $_SESSION=$_POST;
  ?>

  <body>
    <h1> 修正確認</h1>
    <form action="list.php" method="post">
      <input type="hidden" name="id" value="<?php echo $_SESSION['id'] ;?>">
      <label for="">Name:</label>
      <?php echo htmlspecialchars($_SESSION["name"], ENT_QUOTES, "UTF-8"); ?><br>
      <label for="">Email:</label>
      <?php echo htmlspecialchars($_SESSION["email"], ENT_QUOTES, "UTF-8"); ?><br>
      <a href="list.php"><button type="button">もどる</button></a>　
      <input type="submit"  name="edit" value="修正する">
    </form>
  </body>
</html>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>問い合わせフォーム</title>
  </head>

  <?php
  session_start();
  ?>


  <body>
    <h1> お問い合わせフォーム</h1>
    <form action='confirm.php' method='POST'>

      <p>名前を入力してください<span class="error">*<br></p>
      <span class="error"><?php if(isset($_SESSION["nameErr"])){echo $_SESSION["nameErr"];}?></span>
      <p><input type="text" name="name"value="<?php if(!empty($_SESSION['name'])){ echo $_SESSION['name']; } ?>"><br></p>


      <p>性別を選択してください<span class="error">*<br></p>
      <p><select name="gender">
      <span class="error"><?php if(isset($_SESSION["genderErr"])){echo $_SESSION["genderErr"];}?></span>
      <option value="男性">男性
      <option value="女性">女性
      </select><br></p>

      <p>メールアドレスを入力してください<span class="error">*<br></p>
      <span class="error"><?php if(isset($_SESSION["emailErr"])){echo $_SESSION["emailErr"];}?></span>
      <p><input type="text" name="email" value="<?php if(!empty($_SESSION['email'])){ echo $_SESSION['email'];}?>"><br></p>

      <p>内容を入力してください<span class="error">*<br></p>
      <span class="error"><?php if(isset($_SESSION["contentErr"])){echo $_SESSION["contentErr"];}?></span>
      <textarea rows=5 name="content"><?php if(!empty($_SESSION['content'])){ echo $_SESSION['content'];}?></textarea><br>



      <input type="submit"  name="send" value="送信する">
    </form>
  </body>
</html>

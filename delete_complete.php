<!DOCTYPE html>
<html lang="ja">
  <head>
    <style>
    .error {color: #FF0000;}
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>削除完了</title>
  </head>

  <?php
  require "db.php";
  session_start();
  if (empty($_SESSION["id"])||empty($_SESSION["name"])||empty($_SESSION["email"])){
    header('location: list.php');
  }
  deletedb();
  ?>

  <body>
    <h1> 削除完了</h1>
    <h2> 削除しました</h2>
    <a href="list.php"><button type="button">一覧画面へ</button></a>
    </form>
  </body>
</html>
